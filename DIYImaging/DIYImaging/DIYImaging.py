import sys

import numpy as np
import cv2
from PIL import Image, ImageEnhance, ImageOps

class ColorGradientPoint(object):
    def __init__(self, p_r, p_g, p_b, p_v):
        self._r, self._g, self._b = p_r, p_g, p_b
        self._v = p_v #position of the color along the gradient (0 to 1)

class ColorGradient(object):
    def __init__(self):
        self._colorGradientPtArr = []

    def AddGradientColorPoint(self, p_r, p_g, p_b, p_v):
        colorPoint = ColorGradientPoint(p_r, p_g, p_b, p_v)
        for ci in range(len(self._colorGradientPtArr)):
            c = self._colorGradientPtArr[ci]
            if p_v < c._val:
                self._colorGradientPtArr.insert(ci, colorPoint)
                return
        self._colorGradientPtArr.Append(colorPoint)

    def ClearGradient(self):
        self._colorGradientPtArr = []

    def GradientCx(self, p_posArr=[0.0, 0.05, 0.25, 0.50, 0.75, 0.95, 1.0]):
        #p_blue=
        #p_cyan=
        #p_green=
        #p_yellow=
        #p_red=
        ramDac = np.array([
            [0.0, 0.0, 0.5],
            [0.0, 0.0, 1.0],
            [0.0, 1.0, 1.0],
            [0.0, 1.0, 0.0],
            [1.0, 1.0, 0.0],
            [1.0, 0.0, 0.0],
            [0.5, 0.0, 0.0],
        ])
        self._colorGradientPtArr = [ColorGradientPoint(ramDac[i][0], ramDac[i][1], ramDac[i][2], p_posArr[i]) for i in range(len(p_posArr))]

    def GradientColorLookupCx(self, p_posArr=[0.0, 0.125, 0.25, 0.375, 0.5, 0.6, 0.8, 0.90, 1.0]):
        #p_blue=
        #p_cyan=
        #p_green=
        #p_yellow=
        #p_red=
        ramDac = np.array([
            [255.0, 240, 219],
            [0.0, 99.0, 153.0],
            [255.0, 240, 219],
            [0.0, 99.0, 153.0],
            [201.0, 38.0, 90.0],
            [255.0, 240, 219],
            [0.0, 99.0, 153.0],
            [201.0, 38.0, 90.0],
            [255.0, 240, 219],
        ])/255.0
        self._colorGradientPtArr = [ColorGradientPoint(ramDac[i][0], ramDac[i][1], ramDac[i][2], p_posArr[i]) for i in range(len(p_posArr))]

    def GradientBarGrayCx(self, p_width, p_height):
        #m = (255-0)/(p_width-0)
        m = (1.0-0.0)/(p_width-0.0)
        r = np.array([range(p_width),]*p_height)
        img = (m*r)#.astype(dtype=np.uint8)
        return img
    
    def GradientColorAtValueRx(self, p_v, p_heatImg):

        colorGradientPointArrLen = len(self._colorGradientPtArr)
        if colorGradientPointArrLen <= 0:
            return (r,g,b)

        heatImgMask = np.ones_like(p_v, dtype=np.bool)

        for ci in range(colorGradientPointArrLen):
            colorGradientPt = self._colorGradientPtArr[ci]              #current gradient color pt
            colorGradientPt0 = self._colorGradientPtArr[max(0,ci-1)]    #previous gradient color pt
            dv = (colorGradientPt0._v - colorGradientPt._v)

            colorGradientPtIdx = p_v < colorGradientPt._v

            f = np.zeros_like(p_v) if dv == 0.0 else (p_v - colorGradientPt._v) / dv

            r = (colorGradientPt0._r - colorGradientPt._r) * f + colorGradientPt._r
            g = (colorGradientPt0._g - colorGradientPt._g) * f + colorGradientPt._g
            b = (colorGradientPt0._b - colorGradientPt._b) * f + colorGradientPt._b

            p_heatImg[:,:,2][heatImgMask] = r[heatImgMask]
            p_heatImg[:,:,1][heatImgMask] = g[heatImgMask]
            p_heatImg[:,:,0][heatImgMask] = b[heatImgMask]

            heatImgMask[colorGradientPtIdx] = False

#hue is from -180 to 180
def Hue(p_img, p_hue):
    imgHsv = cv2.cvtColor(p_img, cv2.COLOR_BGR2HSV_FULL)

    h = imgHsv[:,:,0]/255.0
    h = (h + p_hue/360.0) % 1.0
    h = cv2.normalize(h, h, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    imgHsv[:,:,0] = h
    img = cv2.cvtColor(imgHsv, cv2.COLOR_HSV2BGR_FULL)
    return img

#hue is from -180 to 180
def Saturation(p_img, p_saturation):
    # 0.5, 1.0, and 2.0 =  -50, 0, and 100
    img = Image.fromarray(p_img)
    converter = ImageEnhance.Color(img)
    sat = (p_saturation+100.0)/100.0
    img = converter.enhance(sat)
    img = np.array(img)
    return img

def Brightness(p_img, p_brightness):    
    img = Image.fromarray(p_img)
    converter = ImageEnhance.Brightness(img)
    fac = (p_brightness+127.0)/127.0
    img = converter.enhance(fac)
    img = np.array(img)
    return img

def Sharpness(p_img, p_sharpness):    
    img = Image.fromarray(p_img)
    converter = ImageEnhance.Sharpness(img)
    fac = (p_sharpness+100.0)/100.0
    img = converter.enhance(fac)
    img = np.array(img)
    return img

def Contrast(p_img, p_contrast):    
    img = Image.fromarray(p_img)
    converter = ImageEnhance.Contrast(img)
    fac = (p_contrast+127.0)/127.0
    img = converter.enhance(fac)
    img = np.array(img)
    return img

def ColorRotation(p_img, p_val):
    return Hue(p_img, p_val)

def Invert(p_img):
    img = np.bitwise_not(p_img)
    return img

def Solarize(p_img):
    #solarization_const = 2.0*np.pi/255.0
    #look_up_table = np.ones((256, 1), dtype = 'uint8' ) * 0
    #for i in range(256):
    #    look_up_table[i][0] = np.abs(np.sin(i*solarization_const))*128
    #img = cv2.LUT(p_img, look_up_table)
    img = Image.fromarray(p_img)

    img = ImageOps.solarize(img)

    #this here is just some pixlr weirdness to make it look like the website
    converter = ImageEnhance.Brightness(img)
    brgt = 2
    img = converter.enhance(brgt)
    converter = ImageEnhance.Contrast(img)
    cont = 1.25
    img = converter.enhance(cont)
    img = Invert(img)

    img = np.array(img)
 
    return img

def CrossProcess(p_img):

    # Define an S shape segmoid that with controlled shape. Based on http://www.flong.com/texts/code/shapers_exp/

    # Function for sigmoid creation with s shape facor
    def doubleExponentialSigmoid(x, a):
        epsilon = 0.00001
        min_param_a = 0.0 + epsilon
        max_param_a = 1.0 - epsilon
        a = min(max_param_a, max(min_param_a, a))
        a = 1.0 - a # for sensible results
        y = 0
        if x <= 0.5:
            y = ((2.0 * x)**(1.0 / a)) / 2.0
        else:
            y = 1.0 - ((2.0 * (1.0-x))**(1.0 / a)) / 2.0
        return y

    # Function for reverse sigmoid creation with reverse s shape facor
    def doubleExponentialSeat(x,a):

        epsilon = 0.00001
        min_param_a = 0.0 + epsilon
        max_param_a = 1.0 - epsilon
        a = min(max_param_a, max(min_param_a, a))
        y = 0
        if x <= 0.5:
            y = ((2.0*x)**(1-a))/2.0;
        else:
            y = 1.0 - ((2.0*(1.0-x))**(1-a))/2.0
        return y

    # Function for s shape function creation
    def getSigmoidLut(sFactor,reverseShape=False):
        rangeOfValues = np.arange(0.0, 1.0 + (1.0/255.0), 1.0/255.0)
        index = 0
        sigmoidLUT = np.zeros_like(rangeOfValues)
        if reverseShape:
            for v in rangeOfValues:
                sigmoidLUT[index] = doubleExponentialSeat(v, sFactor)
                index = index + 1
        else:
            for v in rangeOfValues:
                sigmoidLUT[index] = doubleExponentialSigmoid(v, sFactor)
                index = index + 1

        return sigmoidLUT

    # A function to map one range to another
    def rangeMapping(currentMin,currentMax,newMin,newMax):

        newRange = np.zeros((256,1))
        for v in range(256):
            newRange[v] = (((v - currentMin) * (newMax - newMin)) / (currentMax - currentMin)) + newMin

        return newRange

    # Function to lower contrast by a factor
    def lowerContrast(intensityChannel, factor):

        # Second chane the contrast by the factor
        mappingLUT = rangeMapping(np.min(intensityChannel),np.max(intensityChannel),np.round(np.min(intensityChannel)*factor),np.round(np.max(intensityChannel)/factor))
        newIntensity = cv2.LUT(intensityChannel,mappingLUT)

        return newIntensity

    # This cross processing is based on the tutorial in http://photographypla.net/cross-processed-lightroom/

    # Params
    sBlueFactor = 0.3
    sGreenFactor = 0.3
    sRedFactor = 0.7
    
    lowContrastFactor = 1.05

    # Read image
    #img = cv2.imread('im.jpg')

    # Step 1: Separate to the three channels
    B,G,R = cv2.split(p_img)

    # Step 2: Map to a S curve each channel

    # Get a S shaped segmoid
    blueChannelLUT = np.round(getSigmoidLut(sBlueFactor,True)*255.0).astype(np.uint8)
    greenChannelLUT = np.round(getSigmoidLut(sGreenFactor,False)*255.0).astype(np.uint8)
    redChannelLUT = np.round(getSigmoidLut(sRedFactor,False)*255.0).astype(np.uint8)
    #greenChannelLUT = redChannelLUT

    #B = (B/1.25).astype(np.uint8)
    #G = (G/1.15).astype(np.uint8)
    R = (R/1.25).astype(np.uint8)
    
    # Apply correction
    redChannelCorrection = cv2.LUT(R, redChannelLUT)
    greenChannelCorrection = cv2.LUT(G, greenChannelLUT)
    blueChannelCorrection = cv2.LUT(B, blueChannelLUT)

    # Step 3: Merge corrected channels
    iCorrection = cv2.merge((blueChannelCorrection,greenChannelCorrection,redChannelCorrection))

    # From here you can do whatever you want to the colors shadows highlights etc...
    # Separate color and intensity
    iycr = cv2.cvtColor(iCorrection,cv2.COLOR_BGR2YCR_CB)
    intensityCh,C,R = cv2.split(iycr)

    # Step 4: lower contrast
    newLowerIntensityContrast = lowerContrast(intensityCh, lowContrastFactor)

    # Step 5: Local contrast enhacment
    clahe = cv2.createCLAHE(clipLimit=2.5, tileGridSize=(8,8))
    iCorrectedShadows = clahe.apply(newLowerIntensityContrast.astype(np.uint8))

    # Final step re construct image
    iycrLowContrast = cv2.merge((iCorrectedShadows,C,R))
    finalImage = cv2.cvtColor(iycrLowContrast,cv2.COLOR_YCR_CB2BGR)

    #cv2.imshow('Original',img)
    #cv2.imshow('ColorCorrection',ICorrection)
    #cv2.imshow('LowContrast',newLowerIntensityContrast.astype(np.uint8))
    #cv2.imshow('Final',finalImage)

    img = finalImage

    #img = Hue(img, 3)

    img = Brightness(img, 25)
    img = Contrast(img, -35)
    img = Saturation(img, 10)

    return img

def HeatMap(p_img, p_posArr):#p_blue, p_cyan, p_green, p_yellow, p_red, p_brown

    #import matplotlib.pyplot as plt

    #imgGray = cv2.cvtColor(p_img, cv2.COLOR_BGR2GRAY)

    coefficients = [0.014, 0.087, 0.900] # Gives blue channel all the weight
    # for standard gray conversion, coefficients = [0.114, 0.587, 0.299]
    m = np.array(coefficients).reshape((1,3))
    imgGray = cv2.transform(p_img, m)

    p_img = p_img/255.0
    imgGray = imgGray/255.0

    colorGradient = ColorGradient()
    colorGradient.GradientCx(p_posArr)
    imgGradientBarGray = colorGradient.GradientBarGrayCx(256,64)

    imgGradientBarHeat = np.zeros((imgGradientBarGray.shape[0], imgGradientBarGray.shape[1],3))
    colorGradient.GradientColorAtValueRx(imgGradientBarGray, imgGradientBarHeat);
    imgGradientBarHeat = cv2.normalize(imgGradientBarHeat, imgGradientBarHeat, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

    imgHeat = np.zeros_like(p_img)
    colorGradient.GradientColorAtValueRx(imgGray, imgHeat);
    imgHeat = cv2.normalize(imgHeat, imgHeat, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

    #imgHeat = plt.imshow(imgGray, cmap='jet', interpolation='nearest')
    #plt.axis('off')
    #plt.show()

    #imgGradientBarHeat = plt.imshow(imgGradientBarGray, cmap='jet', interpolation='nearest')
    #plt.axis('off')
    #plt.show()

    return imgHeat,imgGradientBarHeat

def ColorLookup(p_img, p_posArr):

    imgGray = cv2.cvtColor(p_img, cv2.COLOR_BGR2GRAY)/255.0

    colorGradient = ColorGradient()
    colorGradient.GradientColorLookupCx(p_posArr)
    imgGradientBarGray = colorGradient.GradientBarGrayCx(256,64)

    imgGradientBarColorLookup = np.zeros((imgGradientBarGray.shape[0], imgGradientBarGray.shape[1],3))
    colorGradient.GradientColorAtValueRx(imgGradientBarGray, imgGradientBarColorLookup);
    imgGradientBarColorLookup = cv2.normalize(imgGradientBarColorLookup, imgGradientBarColorLookup, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

    imgColorLookup = np.zeros_like(p_img, dtype=float)
    colorGradient.GradientColorAtValueRx(imgGray, imgColorLookup);
    imgColorLookup = cv2.normalize(imgColorLookup, imgColorLookup, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

    return imgColorLookup,imgGradientBarColorLookup

def HistogramEqualization(p_img):#contrast limited adaptive histogram equalization
    lab = cv2.cvtColor(p_img, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    l = cv2.equalizeHist(l)
    limg = cv2.merge((l,a,b))
    img = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)

    return img

def AdaptiveEqualizer(p_img):#contrast limited adaptive histogram equalization
    lab = cv2.cvtColor(p_img, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=0.5, tileGridSize=(8,8))
    cl = clahe.apply(l)
    limg = cv2.merge((cl,a,b))
    img = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)

    img = Hue(img, 5)
    img = Brightness(img, 10)
    img = Contrast(img, -30)
    img = Saturation(img, 15)

    return img
    
def FotoFlexr(p_img, p_pathFileOut, p_debug):

    imgH = Hue(p_img, -180)#-180 - 180
    
    if (p_debug and not p_pathFileOut is None):
        cv2.imwrite(p_pathFileOut + '_H' + outFileExt, imgH)

        imgS = Saturation(p_img, 100) #-100 - 100
        cv2.imwrite(p_pathFileOut + '_S' + outFileExt, imgS)

    imgHs = Saturation(imgH, 100)
    if (p_debug and not p_pathFileOut is None): cv2.imwrite(p_pathFileOut + '_Hs' + outFileExt, imgHs)
    
    posArr = [dblue, blue, cyan, green, yellow, red, dred] = [
        0.0,    #dblue
        0.15,   #blue
        0.35,   #cyan
        0.50,   #green
        0.65,   #yellow
        0.88,   #red
        1.0     #dred
    ]
    imgHeat,imgGradientBarHeat = HeatMap(imgHs, posArr)
    
    if (p_debug and not p_pathFileOut is None):
       cv2.imwrite(p_pathFileOut + '_Hs_Heat' + outFileExt, imgHeat)
       cv2.imwrite(p_pathFileOut + '_GradientBarHeat' + outFileExt, imgGradientBarHeat)

       imgColorRot = ColorRotation(p_img, 240) #0-360
       cv2.imwrite(p_pathFileOut + '_ColorRot' + outFileExt, imgColorRot)

    imgColorRot = ColorRotation(imgHeat, 240)
    if (p_debug and not p_pathFileOut is None):
        cv2.imwrite(p_pathFileOut + '_Hs_Heat_ColorRot' + outFileExt, imgColorRot)

        ##imgInvert = Invert(p_img)
        ##cv2.imwrite(p_pathFileOut + '_Invert' + outFileExt, imgInvert)

        ##imgInvert = Invert(imgColorRot)    
        ##cv2.imwrite(p_pathFileOut + '_Hs_Heat_ColorRot_Invert' + outFileExt, imgInvert)
   
    imgFotoFlxr = imgColorRot
    cv2.imwrite(p_pathFileOut + 'FotoFlxr' + outFileExt, imgFotoFlxr)
    return imgFotoFlxr

def Pixlr(p_img, p_pathFileOut, p_debug):

    imgInvert = Invert(p_img)
    if (p_debug and not p_pathFileOut is None): cv2.imwrite(p_pathFileOut + '_Invert' + outFileExt, imgInvert)

    imgSolarize = Solarize(p_img)
    if (p_debug and not p_pathFileOut is None): cv2.imwrite(p_pathFileOut + '_Solarize' + outFileExt, imgSolarize)
    imgCrossProcess = CrossProcess(p_img)
    if (p_debug and not p_pathFileOut is None): cv2.imwrite(p_pathFileOut + '_CrossProcess' + outFileExt, imgCrossProcess)

    imgSolarize = Solarize(imgInvert)

    posArr = [
        0.0, 0.125, 0.25, 0.375, 0.5, 0.6, 0.8, 0.90, 1.0
    ]

    if (p_debug and not p_pathFileOut is None):
        cv2.imwrite(p_pathFileOut + '_Invert_Solarize' + outFileExt, imgSolarize)    
        imgColorLookup,imgGradientBarColorLookup = ColorLookup(p_img, posArr)
        cv2.imwrite(p_pathFileOut + '_ColorLookup' + outFileExt, imgColorLookup)
        cv2.imwrite(p_pathFileOut + '_GradientBarColorLookup' + outFileExt, imgGradientBarColorLookup)

    imgCrossProcess = CrossProcess(imgSolarize)
    if (p_debug and not p_pathFileOut is None): cv2.imwrite(p_pathFileOut + '_Invert_Solarize_CrossProcess' + outFileExt, imgCrossProcess)

    imgColorLookup,imgGradientBarColorLookup = ColorLookup(imgCrossProcess, posArr)
    if (p_debug and not p_pathFileOut is None): cv2.imwrite(p_pathFileOut + '_Invert_Solarize_CrossProcess_ColorLookup' + outFileExt, imgColorLookup)

    imgPixlr = imgColorLookup
    cv2.imwrite(p_pathFileOut + 'Pixlr' + outFileExt, imgPixlr)
    return imgPixlr

def Lunapic(p_img, p_pathFileOut, p_debug):
    imgAdaptiveEqualizer = AdaptiveEqualizer(p_img)
    if (p_debug and not p_pathFileOut is None): cv2.imwrite(p_pathFileOut + '_AdaptiveEqualizer' + outFileExt, imgAdaptiveEqualizer)
    
    imgSharpness = Sharpness(imgAdaptiveEqualizer, 400)

    imgSaturation = Saturation(imgSharpness, 100)

    imgContrast = Contrast(imgSaturation, 100)

    if (p_debug and not p_pathFileOut is None):
        cv2.imwrite(p_pathFileOut + '_AdaptiveEqualizer_Sharpness' + outFileExt, imgSharpness)
        cv2.imwrite(p_pathFileOut + '_AdaptiveEqualizer_Sharpness_Saturation' + outFileExt, imgSaturation)
        cv2.imwrite(p_pathFileOut + '_AdaptiveEqualizer_Sharpness_Saturation_Contrast' + outFileExt, imgContrast)

        imgSharpness = Sharpness(p_img, 400)
        cv2.imwrite(p_pathFileOut + '_Sharpness' + outFileExt, imgSharpness)

    #this gradient must be fiddled with for a more aligned thermal effect with the lunapic website
    posArr = [dblue, blue, cyan, green, yellow, red, dred] = [
        0.0,    #dblue
        0.15,   #blue
        0.35,   #cyan
        0.50,   #green
        0.65,   #yellow
        0.88,   #red
        1.0     #dred
    ]
    imgThermal,imgGradientBarThermal = HeatMap(imgContrast, posArr)
    
    if (p_debug and not p_pathFileOut is None):
       cv2.imwrite(p_pathFileOut + '_Thermal' + outFileExt, imgThermal)
       cv2.imwrite(p_pathFileOut + '_GradientBarThermal' + outFileExt, imgGradientBarThermal)

    imgHdr = AdaptiveEqualizer(imgThermal)
    if (p_debug and not p_pathFileOut is None): cv2.imwrite(p_pathFileOut + '_Hdr' + outFileExt, imgHdr)

    imgAdjust = Contrast(imgHdr, -100)
    imgAdjust = Brightness(imgAdjust, -100)
    imgAdjust = Contrast(imgHdr, 50)

    b,g,r = cv2.split(imgAdjust)
    #b,r,g -> g,r,b -> g,b,r
    imgSwap = cv2.merge((g,b,r))
    
    imgNormalize = HistogramEqualization(imgSwap)

    imgLunapic = imgNormalize
    cv2.imwrite(p_pathFileOut + 'Lunapic' + outFileExt, imgLunapic)

if __name__ == '__main__':

    if len(sys.argv) > 1:
        pathIn = sys.argv[1]
        fileNameWithExtIn = sys.argv[2]
        pathOut = sys.argv[3]
        fileNameOut = sys.argv[4]
    else:
        pathIn = './In/'
        fileNameWithExtIn = 'cat.jpg'
        pathOut = './Out/'
        fileNameOut = 'img'

    debug = True

    outFileExt = '.png'

    pathFileIn = pathIn + fileNameWithExtIn
    pathFileOut = pathOut + fileNameOut

    #try:
    img = cv2.imread(pathFileIn, cv2.IMREAD_UNCHANGED)

    FotoFlexr(img, pathFileOut, True)

    Pixlr(img, pathFileOut, False)

    Lunapic(img, pathFileOut, False)
